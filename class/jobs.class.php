<?php

class jobs{

  public $min = date('i');
  public $h = date('H');
  public $dom = date('d');
  public $mname = ['enero','febrero','marzo','abril','mayo','junio','julio','agosto','septiembre','octubre','noviembre','diciembre'];
  public $mnumber = date('m');

  public function getMinutes(){ //Get the current minutes.

    echo '<div class="form-group">';
    echo '<label for="minutes">Minuto</label>';
      echo '<select class="form-control" id="minutes" name="minutos">';
      for ($i=0; $i <= 59; $i++) {
        if ($i == $this->$min) {
          echo "<option value=".$i." selected>".$i."</option>";
        }else{
          echo "<option value=".$i.">".$i."</option>";
        }
      }
      echo '</select>';
    echo '</div>';
  }

  public function getHours(){ //Get the current hour.

    echo '<div class="form-group">';
    echo '<label for="hora">Hora</label>';
      echo '<select class="form-control" id="hora" name="hora">';
        for ($i=0; $i <= 23; $i++) {
          if ($i == $this->$h) {
            echo "<option value=".$i." selected>".$i."</option>";
          }else{
            echo "<option value=".$i.">".$i."</option>";
          }
        }
      echo '</select>';
    echo '</div>';
  }

  public function getDom(){ //Get day (number) of the current month.

    echo '<div class="form-group">';
    echo '<label for="dom">Día del mes</label>';
      echo '<select class="form-control" id="dom" name="dom">';
          for ($i=0; $i <= 31; $i++) {
            if ($i == $this->$dom) {
              echo "<option value=".$i." selected>".$i."</option>";
            }else{
              echo "<option value=".$i.">".$i."</option>";
            }
          }
      echo '</select>';
    echo '</div>';
  }

  public function getMonth(){ //Get name of the current month.

    echo '<div class="form-group">';
    echo '<label for="month">Mes actual: ('.$this->mname[($this->$mnumber)-1].')</label>';
      echo '<select class="form-control" id="month" name="mes">';
          for ($i=0; $i < count($this->mname); $i++) {
            if ($i+1 == $this->$mnumber) {
              echo "<option value=".($i+1)." selected>".$this->mname[$i]." (mes actual)</option>";
            }else{
              echo "<option value=".($i+1).">".$this->mname[$i]."</option>";
            }
          }
          echo '</select>';
          echo '</div>';
  }

}

?>
