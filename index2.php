<?php include 'includes/config.inc.php'; ?>
<!DOCTYPE HTML>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>SVPI</title>
  <link rel="icon" type="image/vnd.microsoft.icon" href="images/favicon.ico">
  <!-- CSS LINKS -->
  <link href="css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <link href="css/styles.css" rel="stylesheet">

  <!-- JS SRCS -->
  <script src="js/jquery-1.11.1.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="css/fontawesome.css">
</head>

<body>
  <div class="container">

    <!-- header -->

    <div class="row">
      <div class="col-md-12 site-heading">

              <!-- logo -->
              <h3><img src="images/fulllogo.png" style="opacity:0.8;" width="300px"></h3>
              <br>
              <!-- != logo -->

              <!-- menu -->
              <nav class="navbar navbar-inverse">
                <div class="container-fluid">
                  <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#"><?php echo NAME; ?></a>
                  </div>
                  <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                      <li class="active"><a href="index.php">Canales</a></li>
                      <li><a href="jobs.php">Tareas programadas</a></li>
                      <li><a href="http://cay-svpi1.telecablecartaya.net/svpi">Salir</a></li>
                    </ul>
                  </div>
                </div>
              </nav>
              <!-- =! menu -->

              <!-- breadcrumb -->
              <ul class="breadcrumb">
                <li><a href="#">Menu</a></li>
                <li class="active">Canales</li>
              </ul>
              <!-- != breadcrumb -->

              <p><?php echo NAME; ?> - Switching video with Raspberry PI </p>
              <div class="border"></div>
          </div>
    </div>

    <!-- != header -->

    <br>

    <!-- lock state -->

    <div id='simulcrypt'></div>

    <!-- != lock state -->

    <!-- events area -->

    <div class="input-group">
        <div class="field span3" >
            <textarea id="estado" class="form-control" rows="5" cols="200" readonly>Mostrando canal:</textarea>
        </div>
    </div>

    <!-- != events area -->

    <!-- channels -->

    <div class="row">
              <div class="col-xs-3 col-sm-3 col-md-3 nopadding">
                <div class="col-md-12 feature-box">
                  <i class="fas fa-power-off icon icon-number"></i>
                  <h4>GPIO</h4>
                  <p>Estado de la GPIO: </p>
                  <br>
                  <br>
                  <div class="material-switch row">
                    <div class="col-md-12 button-position">
                      <input id="someSwitchOptionPrimary" name="someSwitchOption001" type="checkbox"/>
                      <label for="someSwitchOptionPrimary" class="label-primary"></label>
                    </div>
                                    </div>
                </div>
              </div><!-- End Col -->
              <div class="col-xs-3 col-sm-3 col-md-3">
                  <div class="col-md-12 feature-box ';if ($entradaactual == "IN1") echo "alert-info"; echo '">
                  <span class="icon number">1</span>
                  <h4>Canal 1</h4>
                  <p>Entrada 1 de la placa.</p>
                  <p><?php echo CANAL1_DETAIL;?></p>
                  <br>
                  <br>
                  <div class="material-switch row">
                          <div class="col-md-12 button-position">
                                  <input id="canal1" name="in1" type="checkbox" class="canales" onClick="CambiaCanal(\'cambiar.php?opc=canal0\', \'Cambiando Canal...\')" '; if ($entradaactual == "IN1") echo "checked disabled"; echo '/>
                                  <label for="canal1" class="label-primary"></label>
                          </div>
                  </div>
                </div>
              </div> <!-- End Col -->

              <div class="col-xs-3 col-sm-3 col-md-3">
                  <div class="feature-box ';if ($entradaactual == "IN2") echo "alert-info"; echo '">
                  <span class="icon number">2</span>
                  <h4>Canal 2</h4>
                  <p>Entrada 2 de la placa.</p>
                  <p><?php echo CANAL2_DETAIL;?></b></p>
                  <br>
                  <br>
                  <div class="material-switch row">
                          <div class="col-md-12 button-position">
                                  <input id="canal2" name="in2" type="checkbox" class="canales" onClick="CambiaCanal(\'cambiar.php?opc=canal1\', \'Cambiando Canal...\')" ';
                                  if ($entradaactual == "IN2") {
                                    echo "checked disabled";
                                  }else{
                                    echo $entradaactual;
                                  }
                                  echo '/><label for="canal2" class="label-primary"></label>
                          </div>
                  </div>
                  </div>
              </div> <!-- End Col -->
              <div class="col-xs-3 col-sm-3 col-md-3">
              <div class="feature-box ';if ($entradaactual == "IN3") echo "alert-info"; echo '">
              <span class="icon number">3</span>
              <h4>Canal 3</h4>
              <p>Entrada 3 de la placa.</p>
              <p><?php echo CANAL3_DETAIL;?></p>
              <br>
              <br>
              <div class="material-switch row">
                      <div class="col-md-12 button-position">
                              <input id="canal3" name="in3" type="checkbox" class="canales" onClick="CambiaCanal(\'cambiar.php?opc=canal2\', \'Cambiando Canal...\')" '; if ($entradaactual == "IN3") echo "checked disabled"; echo '/>
                              <label for="canal3" class="label-primary"></label>
                      </div>
              </div>
              </div>
              </div> <!-- End Col -->

            </div>
          </div>
        </div>

</body>

</html>


<script>
$(document).ready(function() {

  function update() {
    $.ajax({
     type: 'POST',
     url: 'estadoEncrip.php',
     timeout: 1000,
     success: function(data) {
        $("#simulcrypt").html(data);
        window.setTimeout(update, 1000);
     }
    });
   }
   update();

});
</script>
