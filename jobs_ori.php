<?php include 'load.php'; ?>
<?php

require("func.inc.php");
$con = conexion_ssh();
echo '<link href="styles.css" rel="stylesheet">
<link rel="icon" type="image/vnd.microsoft.icon" href="favicon.ico">
<title>SVPI - Switching video with Raspberry PI</title>
<link href="css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->';

echo '<div class="Features-section paddingTB60 ">
    <div class="container">
    <link href="css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery-1.11.1.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <link href="styles.css" rel="stylesheet">
    <script>
    $(document).ready(function() {

      function update() {
        $.ajax({
         type: \'POST\',
         url: \'datetime.php\',
         timeout: 1000,
         success: function(data) {
            $("#timer").html("Fecha / Hora: "+data);
            window.setTimeout(update, 1000);
         }
        });
       }
       update();

    });
    </script>
  <div class="row">
    <div class="col-md-12 site-heading ">
            <h3><img src="logo_completo.png" style="opacity:0.8;" width="300px"></h3>
            <!------ Include the above in your HEAD tag ---------->
            <br>
            <nav class="navbar navbar-inverse">
              <div class="container-fluid">
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                  <a class="navbar-brand" href="#">TCC1</a>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                  <ul class="nav navbar-nav">
                    <li><a href="index.php">Canales</a></li>
                    <li class="active"><a href="jobs.php">Tareas programadas</a></li>
                    <li><a href="http://cay-svpi1.telecablecartaya.net/svpi">Salir</a></li>
                  </ul>
                </div>
              </div>
            </nav>
            <ul class="breadcrumb">
              <li><a href="#">Menu</a></li>
              <li class="active">Tareas programadas</li>
            </ul>
            <ul class="breadcrumb">
              <li id="timer"></li>
            </ul>
            ';
            if (isset($_POST['envio'])) {
              insertarTarea($con, $_POST['minutos'], $_POST['hora'], $_POST['dom'], $_POST['mes'], $_POST['dow'], $_POST['canal'], $_POST['encriptado']);
              insertarEncriptacion($con, $_POST['minutos'], $_POST['hora'], $_POST['dom'], $_POST['mes'], $_POST['dow'], $_POST['canal'], $_POST['encriptado']);
              echo '<div class="alert alert-success alert-dismissible" role="alert">
                      Nueva tarea creada
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>';
            }
            if (isset($_POST['borrarTareas'])) {
              limpiarTareas($con);
              echo '<div class="alert alert-success alert-dismissible" role="alert">
                      Tareas borradas
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>';
            }

echo '<br><!-- Button trigger modal -->
<button type="button" class="btn btn-danger pull-right" data-toggle="modal" data-target="#borrar">Limpiar todas las tareas</button>

<!-- Modal -->
<div class="modal fade" id="borrar" tabindex="-1" role="dialog" aria-labelledby="borrarLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="BorrarLabel">Borrar tareas</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      ¿Desea borrar todas las tareas?
      </div>
      <div class="modal-footer">
      <form method="post" action="jobs.php">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <input type="submit" class="btn btn-danger" name="borrarTareas" value="Limpiar la lista de tareas">
      </form>
      </div>
    </div>
  </div>
</div>';

echo '<!-- Button trigger modal -->
<button type="button" class="btn pull-left btn-info" data-toggle="modal" data-target="#exampleModal">
  Programar tarea
</button>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">Programar nueva tarea</h4>
        <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>-->
        </button>
      </div>
      <div class="modal-body">
      <form method="post" action="jobs.php">';
      //Minutos
      echo '<div class="form-group">
            <label for="minutes">Minuto</label>
            <select class="form-control" id="minutes" name="minutos">';
            for ($i=0; $i <= 59; $i++) {
              if ($i == date('i')) {
                echo "<option value=".$i." selected>".$i."</option>";
              }else{
                echo "<option value=".$i.">".$i."</option>";
              }
            }
            echo '</select>
                  </div>';
      //Horas
      echo '<div class="form-group">
            <label for="hora">Hora</label>
            <select class="form-control" id="hora" name="hora">';
            for ($i=0; $i <= 23; $i++) {
              if ($i == date('H')) {
                echo "<option value=".$i." selected>".$i."</option>";
              }else{
                echo "<option value=".$i.">".$i."</option>";
              }
            }
            echo '</select>
                  </div>';
        //Dia del mes
        echo '<div class="form-group">
              <label for="dom">Día del mes</label>
              <select class="form-control" id="dom" name="dom">';
              for ($i=0; $i <= 31; $i++) {
                if ($i == date('d')) {
                  echo "<option value=".$i." selected>".$i."</option>";
                }else{
                  echo "<option value=".$i.">".$i."</option>";
                }
              }
              echo '</select>
                    </div>';
          //Mes
          $mes = ['enero','febrero','marzo','abril','mayo','junio','julio','agosto','septiembre','octubre','noviembre','diciembre'];

          echo '<div class="form-group">
                <label for="dom">Mes actual: ('.$mes[date('m')-1].')</label>
                <select class="form-control" id="dom" name="mes">';
                $mes = ['enero','febrero','marzo','abril','mayo','junio','julio','agosto','septiembre','octubre','noviembre','diciembre'];
                for ($i=0; $i < count($mes); $i++) {
                  if ($i+1 == date('m')) {
                    echo "<option value=".($i+1)." selected>".$mes[$i]." (mes actual)</option>";
                  }else{
                    echo "<option value=".($i+1).">".$mes[$i]."</option>";
                  }
                }
                echo '</select>
                      </div>';
          //Dia de la semana
          $dow = ['lunes','martes','miercoles','jueves','viernes','sabado','domingo'];

          echo '<div class="form-group">
                <label for="dow">Día de la semana actual: ('.$dow[date('w')-1].')</label>
                <select class="form-control" id="dow" name="dow">';
                $dow = ['lunes','martes','miercoles','jueves','viernes','sabado','domingo'];
                for ($i=0; $i < count($dow); $i++) {
                  if ($i+1 == date('w')) {
                    echo "<option value=".($i+1)." selected>".$dow[$i]." (hoy)</option>";
                  }else{
                  echo "<option value=".($i+1).">".$dow[$i]."</option>";
                  }
                }
                echo '</select>
                      </div>';

          //Canal
          echo '<div class="form-group">
                <label for="canal">Canal a cambiar</label>
                <select class="form-control" id="canal" name="canal">
                  <option value="1">Canal 1</option>
                  <option value="2">Canal 2</option>
                  <option value="3">Canal 3</option>
                </select>
                </div>';
          echo '<div class="form-group">
                  <label for="encriptado">¿Encriptar el canal? <i class="fas fa-lock"></i> / <i class="fas fa-lock-open"></i></label>
                  <select class="form-control" id="encriptado" name="encriptado">
                    <option value="1">Si (Encriptar canal)</option>
                    <option value="0">No (Desencriptar canal)</option>
                  </select>
                  </div>';

        echo '</div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <input type="submit" class="btn btn-primary" value="Programar tarea" name="envio">
      </div>
      </form>
    </div>
  </div>
</div><br><br><br>';
echo '<div class="well">
    <table class="table">
      <thead>
        <tr>
          <th>Dia / Hora</th>
          <th>Tarea</th>
          <th>Info</th>
        </tr>
      </thead>
      <tbody>
        ';
        leerTareas($con);
        echo '
      </tbody>
    </table>
</div>
</div>';
?>
