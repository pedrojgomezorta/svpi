<?php include 'includes/config.inc.php'; ?>
<?php include 'html/head.html.php'; ?>
<?php include 'html/menu.html.php'; ?>
<?php menu("Canales"); ?>

		<!--Container-->

    <br>

    <!-- lock state -->

    <div id='simulcrypt'></div>

    <!-- != lock state -->

    <!-- events area -->

    <div class="input-group">
        <div class="field span3" >
            <textarea id="estado" class="form-control" rows="5" cols="200" readonly>Mostrando canal:</textarea>
        </div>
    </div>

    <!-- != events area -->

    <!-- channels -->

    <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-3">
                <div class="col-md-12 feature-box">
                  <i class="fas fa-power-off icon icon-number"></i>
                  <h4>GPIO</h4>
                  <p>Estado de la GPIO: </p>
                  <br>
                  <br>
                  <div class="material-switch row">
                    <div class="col-md-12 button-position">
                      <input id="someSwitchOptionPrimary" name="someSwitchOption001" type="checkbox"/>
                      <label for="someSwitchOptionPrimary" class="label-primary"></label>
                    </div>
                                    </div>
                </div>
              </div><!-- End Col -->
              <div class="col-xs-12 col-sm-12 col-md-3">
                  <div class="col-md-12 feature-box ';if ($entradaactual == "IN1") echo "alert-info"; echo '">
                  <span class="icon number">1</span>
                  <h4>Canal 1</h4>
                  <p>Entrada 1 de la placa.</p>
                  <p><?php echo CANAL1_DETAIL;?></p>
                  <br>
                  <br>
                  <div class="material-switch row">
                          <div class="col-md-12 button-position">
                                  <input id="canal1" name="in1" type="checkbox" class="canales" onClick="CambiaCanal(\'cambiar.php?opc=canal0\', \'Cambiando Canal...\')" '; if ($entradaactual == "IN1") echo "checked disabled"; echo '/>
                                  <label for="canal1" class="label-primary"></label>
                          </div>
                  </div>
                </div>
              </div> <!-- End Col -->

              <div class="col-xs-12 col-sm-12 col-md-3">
                  <div class="feature-box ';if ($entradaactual == "IN2") echo "alert-info"; echo '">
                  <span class="icon number">2</span>
                  <h4>Canal 2</h4>
                  <p>Entrada 2 de la placa.</p>
                  <p><?php echo CANAL2_DETAIL;?></b></p>
                  <br>
                  <br>
                  <div class="material-switch row">
                          <div class="col-md-12 button-position">
                                  <input id="canal2" name="in2" type="checkbox" class="canales" onClick="CambiaCanal(\'cambiar.php?opc=canal1\', \'Cambiando Canal...\')" ';
                                  if ($entradaactual == "IN2") {
                                    echo "checked disabled";
                                  }else{
                                    echo $entradaactual;
                                  }
                                  echo '/><label for="canal2" class="label-primary"></label>
                          </div>
                  </div>
                  </div>
              </div> <!-- End Col -->
              <div class="col-xs-12 col-sm-12 col-md-3">
              <div class="feature-box ';if ($entradaactual == "IN3") echo "alert-info"; echo '">
              <span class="icon number">3</span>
              <h4>Canal 3</h4>
              <p>Entrada 3 de la placa.</p>
              <p><?php echo CANAL3_DETAIL;?></p>
              <br>
              <br>
              <div class="material-switch row">
                      <div class="col-md-12 button-position">
                              <input id="canal3" name="in3" type="checkbox" class="canales" onClick="CambiaCanal(\'cambiar.php?opc=canal2\', \'Cambiando Canal...\')" '; if ($entradaactual == "IN3") echo "checked disabled"; echo '/>
                              <label for="canal3" class="label-primary"></label>
                      </div>
              </div>
              </div>
              </div> <!-- End Col -->

            </div>


<?php include 'html/footer.html'; ?>
<script>
$(document).ready(function() {

  function update() {
    $.ajax({
     type: 'POST',
     url: 'estadoEncrip.php',
     timeout: 1000,
     success: function(data) {
        $("#simulcrypt").html(data);
        window.setTimeout(update, 1000);
     }
    });
   }
   update();

});
</script>
