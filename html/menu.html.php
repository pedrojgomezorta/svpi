<?php

function menu($active){
echo '
<body>
  <div class="container">

    <!-- header -->

    <div class="row">
      <div class="col-md-12 site-heading">

              <!-- logo -->
              <h3><img src="images/fulllogo.png" style="opacity:0.8;" width="300px"></h3>
              <br>
              <!-- != logo -->

              <!-- menu -->
              <nav class="navbar navbar-inverse">
                <div class="container-fluid">
                  <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">'.NAME.'</a>
                  </div>
                  <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                      <li '; if ($active == "Canales") echo 'class="active"'; echo '><a href="index.php">Canales</a></li>
                      <li '; if ($active == "Jobs") echo 'class="active"'; echo '><a href="jobs.php">Tareas Programadas</a></li>
                      <li><a href="login.php">Salir</a></li>
                    </ul>
                  </div>
                </div>
              </nav>
              <!-- =! menu -->

              <!-- breadcrumb -->
              <ul class="breadcrumb">
                <li><a href="#">Menu</a></li>
                <li class="active">Canales</li>
              </ul>
              <!-- != breadcrumb -->

              <p> '.NAME.' - Switching video with Raspberry PI </p>
              <div class="border"></div>
          </div>
    </div>

    <!-- != header -->
'
;
}

?>
