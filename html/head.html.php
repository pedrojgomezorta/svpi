<!DOCTYPE HTML>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>SVPI</title>
  <link rel="icon" type="image/vnd.microsoft.icon" href="images/favicon.ico">
  <!-- CSS LINKS -->
  <link href="css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <link href="css/styles.css" rel="stylesheet">

  <!-- JS SRCS -->
  <script src="js/jquery-1.11.1.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="css/fontawesome.css">
</head>
