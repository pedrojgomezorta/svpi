<?php include 'config.inc.php'; ?>
<?php

$canales = array("Canal 1", "Canal 2", "Canal 3");

function conexion_ssh()
{
        $con = ssh2_connect(SSH_SERVER, SSH_PORT);
        ssh2_auth_password($con, SSH_USER, SSH_PASS);
        return $con;
}

function habilitarGPIO($con)
{
        $datos = ssh2_exec($con, "/root/iniciaCanal", 'xterm');
        stream_set_blocking($datos, true);
        echo stream_get_contents($datos);
}

function apagarGPIO($con)
{
        $datos = ssh2_exec($con, "/root/apagarGPIO", 'xterm');
        stream_set_blocking($datos, true);
        echo stream_get_contents($datos);
}

function get_canalmostrado($con)
{
        $datos = ssh2_exec($con, "cat /root/canalactivo", 'xterm');
        stream_set_blocking($datos, true);

        return stream_get_contents($datos);
}
function encriptarCanal($con){
  require_once "PHPTelnet.php";

  $telnet = new PHPTelnet();

  // if the first argument to Connect is blank,
  // PHPTelnet will connect to the local host via 127.0.0.1
  $result = $telnet->Connect('192.168.0.28','','');

  if ($result == 0) {
  $telnet->DoCommand('=remux.service.scrambler.group_connect(1,0,3)', $result);
  // NOTE: $result may contain newlines
  echo $result;
  // say Disconnect(0); to break the connection without explicitly logging out
  $telnet->Disconnect();
  }
  $datos = ssh2_exec($con, 'echo 1 > /home/svpi/encriptado', 'xterm');
  stream_set_blocking($datos, true);
  echo stream_get_contents($datos);

}
function desencriptarCanal($con){
  require_once "PHPTelnet.php";

  $telnet = new PHPTelnet();

  // if the first argument to Connect is blank,
  // PHPTelnet will connect to the local host via 127.0.0.1
  $result = $telnet->Connect('192.168.0.28','','');

  if ($result == 0) {
  $telnet->DoCommand('=remux.service.scrambler.group_disconnect(1,0)', $result);
  // NOTE: $result may contain newlines
  echo $result;
  // say Disconnect(0); to break the connection without explicitly logging out
  $telnet->Disconnect();
  }
  $datos = ssh2_exec($con, 'echo 0 > /home/svpi/encriptado', 'xterm');
  stream_set_blocking($datos, true);
  echo stream_get_contents($datos);
}
function login()
{
        session_start();
        $sesion_iniciada = 0;


        if (isset($_SESSION['username']))
        {
                $sesion_iniciada = 1;
        }
        else if (isset($_POST['user']) && isset($_POST['pass']))
        {

                /*$lconect = Conectarse('localhost', 'tcc', '1234');

                $sql = "SELECT * FROM usuarios WHERE user = '".$_POST['user']."'";
                $query = mysql_query($sql);

                if ($sesion_iniciada == 0 && $row = mysql_fetch_array($query))
                {
                        if ($_POST['user'] === $row['user'] && sha1($_POST['pass']) === $row['pass'])
                        {
                                $_SESSION['username'] = $row['user'];
                                $sesion_iniciada = 1;
                        }
                }
                if ($sesion_iniciada == 0) echo '<br />Error 104. Fallo al iniciar la sesión.<br />';

                mysql_close($lconect);
                */

                if ($_POST['user'] === 'admin' && sha1($_POST['pass']) === sha1('cartaya'))
                {
                        $_SESSION['username'] = 'admin';
                        $sesion_iniciada = 1;
                }

        }

        return $sesion_iniciada;
}

function form_login()
{
        echo '
        <link href="css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <link href="login.css" rel="stylesheet">
        <script src="js/bootstrap.min.js"></script>
        <link rel="icon" type="image/vnd.microsoft.icon" href="favicon.ico">
        <script src="js/jquery-1.11.1.min.js"></script>
        <!------ Include the above in your HEAD tag ---------->

        <div class="wrapper fadeInDown">
          <div id="formContent">
            <!-- Tabs Titles -->

            <!-- Icon -->
            <div class="fadeIn first">
              <br>
              <img src="logo_sw.png" id="icon" alt="User Icon" style="opacity:0.8;" width="120px" />
              <br><br>
              <p>Switching video with Raspberry PI </p>
              <div class="border"></div>
            </div>
            <br>
            <!-- Login Form -->
            <form name="login" action="" id="login_form" method="POST">
              <input type="text" id="user" class="fadeIn second" name="user" placeholder="Login">
              <input type="password" id="password" class="fadeIn third" name="pass" placeholder="Password">
              <br><br>
              <input type="submit" id="envio" class="fadeIn fourth" name="submit" value="Log In">
            </form>
            <br>
          </div>
        </div>';
        return 0;
}

function countdown($year, $month, $day, $hour, $minute, $second){
    global $return;
    global $countdown_date;
    $countdown_date = mktime($hour, $minute, $second, $month, $day, $year);
    $today = time();
   $diff = $countdown_date - $today;
    if ($diff < 0)$diff = 0;
    $dl = floor($diff/60/60/24);
    $hl = floor(($diff - $dl*60*60*24)/60/60);
    $ml = floor(($diff - $dl*60*60*24 - $hl*60*60)/60);
    $sl = floor(($diff - $dl*60*60*24 - $hl*60*60 - $ml*60));
  // OUTPUT

  $return = array($dl, $hl, $ml, $sl);
  return $return;
}

function leerTareas($con){
          $mes = ['enero','febrero','marzo','abril','mayo','junio','julio','agosto','septiembre','octubre','noviembre','diciembre'];

          $datos = ssh2_exec($con, "cat /etc/cron.d/svpi", 'xterm');
          stream_set_blocking($datos, true);
          while($line = fgets($datos)) {
              //$line = str_replace(" ", "-", $line);
              //echo "<pre>".$line."</pre>";
              $line = preg_replace('/\s+/', '-;', $line);
              //echo "<pre>".$line."</pre>";
              $valores = explode("-;", $line);
              $array = countdown(date("y"), $valores[3], $valores[2], $valores[1], $valores[0], 0);
              list($dl,$hl,$ml,$sl) = $array;
              echo "";
              echo '<tr>
                    <td>'.$valores[2].' de '.$mes[$valores[3]-1].' a las '.$valores[1].':'.$valores[0].'</td>
                    <td>'.substr($valores[7],6).'</td>';
                    if ($dl == 0 && $hl == 0 && $ml == 0 && $sl == 0) {
                      echo "<td>Tarea finalizada</td>";
                    }
                    else{
                      echo "<td>";
                      echo "Quedan ";
                      if ($dl > 0) {
                        echo $dl.' días ';
                      }
                      if ($hl > 0) {
                        echo $hl.' horas ';
                      }
                      if ($ml > 0) {
                        echo $ml.' minutos ';
                      }
                      if ($sl > 0) {
                        echo $sl.' segundos';
                      }
                      echo ' restantes.</td>';
                    }
                       echo '
                  </tr>';

          }
}
function insertarEncriptacion($con, $minuto, $hora, $dom, $month, $dow, $canal, $encriptar){

  if ($encriptar == 0){
    $datos = ssh2_exec($con, 'echo "'.$minuto.'      '.$hora.'      '.$dom.'      '.$month.'      '.$dow.'       root    sh /root/desencriptar" >> /etc/cron.d/svpi', 'xterm'); //Tiene q estar asi por las tabs
    stream_set_blocking($datos, true);
    echo stream_get_contents($datos);
  }
  if ($encriptar == 1){
    $datos = ssh2_exec($con, 'echo "'.$minuto.'      '.$hora.'      '.$dom.'      '.$month.'      '.$dow.'       root    sh /root/encriptar" >> /etc/cron.d/svpi', 'xterm'); //Tiene q estar asi por las tabs
    stream_set_blocking($datos, true);
    echo stream_get_contents($datos);
  }
}
function insertarTarea($con, $minuto, $hora, $dom, $month, $dow, $canal, $encriptar){
  $datos = ssh2_exec($con, 'echo "'.$minuto.'      '.$hora.'      '.$dom.'      '.$month.'      '.$dow.'       root    sh /root/canalIN'.$canal.'" >> /etc/cron.d/svpi', 'xterm'); //Tiene q estar asi por las tabs
  stream_set_blocking($datos, true);
  echo stream_get_contents($datos);
}

function limpiarTareas($con){
  $datos = ssh2_exec($con, '> /etc/cron.d/svpi', 'xterm');
  stream_set_blocking($datos, true);
  echo stream_get_contents($datos);
}

function estadoEncript($con){
        $datos = ssh2_exec($con, "more /root/encriptado", 'xterm');
        stream_set_blocking($datos, true);
        return stream_get_contents($datos);
}
?>
