<?php

require("func.inc.php");

        $con = conexion_ssh();


        if ($_GET['opc'] == 'cambiar')
        {
                $datos = ssh2_exec($con, "/root/cambiarcanal -1", 'xterm');

                stream_set_blocking($datos, true);
                echo stream_get_contents($datos);

                echo 'Mostrando canal '.$canales[get_canalmostrado($con)[0]];
        }
        else if ($_GET['opc'] == 'iniciar')
        {
                habilitarGPIO($con);
        }
        else if ($_GET['opc'] == 'apagar')
        {
                apagarGPIO($con);
        }
        else if ($_GET['opc'] == 'canal0')
        {
                $datos = ssh2_exec($con, "/root/canalIN1", 'xterm');

                stream_set_blocking($datos, true);
                echo stream_get_contents($datos);

                echo 'Mostrando canal '.$canales[get_canalmostrado($con)[0]];
        }
        else if ($_GET['opc'] == 'canal1')
        {
                $datos = ssh2_exec($con, "/root/canalIN2", 'xterm');

                stream_set_blocking($datos, true);
                echo stream_get_contents($datos);

                echo 'Mostrando canal '.$canales[get_canalmostrado($con)[0]];
        }
        else if ($_GET['opc'] == 'canal2')
        {
                $datos = ssh2_exec($con, "/root/canalIN3", 'xterm');

                stream_set_blocking($datos, true);
                echo stream_get_contents($datos);

                echo 'Mostrando canal '.$canales[get_canalmostrado($con)[0]];
        }
        else if ($_GET['opc'] == 'encriptar')
        {
                encriptarCanal($con);
        }
        else if ($_GET['opc'] == 'desencriptar')
        {
                desencriptarCanal($con);
        }
?>
