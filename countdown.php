
<?php

$year = '2018';
$month= '12';
$day = '31';
$hour = '00';
$minute = '00';
$second = '00';
//Countdown Function
function countdown($year, $month, $day, $hour, $minute, $second)
{
    global $return;
    global $countdown_date;
    $countdown_date = mktime($hour, $minute, $second, $month, $day, $year);
    $today = time();
   $diff = $countdown_date - $today;
    if ($diff < 0)$diff = 0;
    $dl = floor($diff/60/60/24);
    $hl = floor(($diff - $dl*60*60*24)/60/60);
    $ml = floor(($diff - $dl*60*60*24 - $hl*60*60)/60);
    $sl = floor(($diff - $dl*60*60*24 - $hl*60*60 - $ml*60));
  // OUTPUT
  echo "Today's date ".date("F j, Y, g:i:s A")."<br/>";
  echo "Countdown date ".date("F j, Y, g:i:s A",$countdown_date)."<br/>";
  echo "\n<br>";
  $return = array($dl, $hl, $ml, $sl);
  return $return;
}

countdown($year, $month, $day, $hour, $minute, $second);
list($dl,$hl,$ml,$sl) = $return;
echo "Quedan ".$dl." days ".$hl." hours ".$ml." minutes ".$sl." seconds left"."\n<br>";
/*
Above snippet produces following output:

Today's date January 12, 2008, XX:XX:XX PM
Countdown date December 31, 2008, 12:00:00 AM

Countdown 353 days XX hours XX minutes XX seconds left
*/
?>
