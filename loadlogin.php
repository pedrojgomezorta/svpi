<link href="css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery-1.11.1.min.js"></script>
<div class="loader">
    <span></span>
    <span></span>
    <span></span>
</div>
<h1></h1>
<style>
body {
    margin:0;
    padding:0;
    background:#fff;
}
h1 {
    position:absolute;
    top:38%;
    left:50%;
    width: 100%;
    height: 100%;
    z-index:-1;
    font-size:70px;
    transform:translate(-50%,-50%);
    color:#101010;
    background: url(logo_sw.png) no-repeat center;
    opacity: 0.8;
}
.loader {
    position:absolute;
    top:50%;
    transform:translateY(-50%);
    width:100%;
    height:10px;
    text-align:center;
}
.loader span {
    width:30px;
    height:30px;
    background:#101010;
    opacity: 0.8;
    display:inline-block;
    border-radius:50%;
    animation:animate 2s linear infinite;
    opacity:0;
}
.loader span:nth-child(1) {
    animation-delay:0.8s;
    background:#f2f2f2;
}
.loader span:nth-child(2) {
    animation-delay:0.4s;
    background:#101010;
    opacity: 0.8;
}
.loader span:nth-child(3) {
    animation-delay:0.2s;
    background:#f1f1f1;
}

@keyframes animate {
    0% {
        transform: translateX(-200px);
        opacity:0;
    }
    25% {
        transform: translateX(-100px);
        opacity:1;
    }
    50% {
        transform: translateX(0);
        opacity:1;
    }
    75% {
        transform: translateX(0);
        opacity:1;
    }
    100% {
        transform: translateX(100px);
        opacity:0;
    }
    90% {
        transform: translateX(100px);
        opacity:0;
    }
}
</style>
