<?php include 'load.php'; ?>
<?php

require("includes/func.inc.php");

        echo '<script>
                function CambiaCanal(url, texto){
                $.ajax({
                        url:   url,
                        beforeSend: function () {
                        $("#estado").html(texto);
                        location.reload();
                        location.reload();
                },
                success: function (response) {
                        $("#estado").html(response);
                        location.reload();
                        location.reload();
                }
        });
        }
        </script>';

        $con = conexion_ssh();

                /*echo '<input type="button" name="apagarGPIO" value="Apagar GPIO" onClick="CambiaCanal(\'cambiar.php?opc=apagar\', \'Apagando GPIO...\');" />';
                echo '<br /><input type="button" value="Cambiar Canal" onClick="CambiaCanal(\'cambiar.php?opc=cambiar\', \'Cambiando Canal...\')" /><br />';
                echo '<br /><input type="button" value="Canal 1" onClick="CambiaCanal(\'cambiar.php?opc=canal0\', \'Cambiando Canal...\')" />';
                echo '<input type="button" value="Canal 2" onClick="CambiaCanal(\'cambiar.php?opc=canal1\', \'Cambiando Canal...\')" />';
                echo '<input type="button" value="Canal 3" onClick="CambiaCanal(\'cambiar.php?opc=canal2\', \'Cambiando Canal...\')" />';*/
                $entradaactual = get_canalmostrado($con);
                $entradaactual = preg_replace("/[\r\n|\n|\r]+/", "", $entradaactual);;//Quitamos los saltos de linea
                //echo '<div id="estado">Mostrando canal '.$canales[get_canalmostrado($con)[0]].'</div>';
                echo '<link href="styles.css" rel="stylesheet">
                <link rel="icon" type="image/vnd.microsoft.icon" href="favicon.ico">
                <title>SVPI - Switching video with Raspberry PI</title>
                <link href="css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
                <script src="js/bootstrap.min.js"></script>
                <script src="js/jquery-1.11.1.min.js"></script>
                <!------ Include the above in your HEAD tag ---------->';

                echo '<div class="Features-section paddingTB60 ">
                    <div class="container">
                    <link href="css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
                    <script src="js/bootstrap.min.js"></script>
                    <script src="js/jquery-1.11.1.min.js"></script>
                    <link rel="stylesheet" href="css/fontawesome.css">

                    <link href="css/styles.css" rel="stylesheet">
                  <div class="row">
                    <div class="col-md-12 site-heading ">
                            <h3><img src="images/logo_completo.png" style="opacity:0.8;" width="300px"></h3>
                            <!------ Include the above in your HEAD tag ---------->
                            <br>
                            <nav class="navbar navbar-inverse">
                              <div class="container-fluid">
                                <div class="navbar-header">
                                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                  </button>
                                  <a class="navbar-brand" href="#">TCC1</a>
                                </div>
                                <div class="collapse navbar-collapse" id="myNavbar">
                                  <ul class="nav navbar-nav">
                                    <li class="active"><a href="index.php">Canales</a></li>
                                    <li><a href="jobs.php">Tareas programadas</a></li>
                                    <li><a href="http://cay-svpi1.telecablecartaya.net/svpi">Salir</a></li>
                                  </ul>
                                </div>
                              </div>
                            </nav>
                            <ul class="breadcrumb">
                              <li><a href="#">Menu</a></li>
                              <li class="active">Canales</li>
                            </ul>
                            <p>TCC1 - Switching video with Raspberry PI </p>
                            <div class="border"></div>
                          </div>
                  </div>
                  <br>';
                  echo "<div id='simulcrypt'></div>";
                  echo '<div class="input-group">
                                <div class="field span3" >
                                    <textarea id="estado" class="form-control" rows="5" cols="200" readonly>Mostrando canal: '.$entradaactual.'</textarea>
                                </div>
                            </div>
                  <div class="row">
                            <!-- <div class="col-xs-4 col-sm-4 col-md-3">
                              <div class="col-md-12 feature-box">
                                <span class="glyphicon glyphicon-off icon"></span>
                                <h4>Activar GPIO</h4>
                                <p>Activa la placa GPIO.</p>
                                <br>
                                <br>
                                <div class="material-switch row">
                                  <div class="col-md-12">
                                                          <input id="someSwitchOptionPrimary" name="someSwitchOption001" type="checkbox"/>
                                                          <label for="someSwitchOptionPrimary" class="label-primary"></label>
                                  </div>
                                                  </div>
                              </div>
                            </div> --> <!-- End Col -->
                            <div class="col-xs-4 col-sm-4 col-md-4">
                                <div class="col-md-12 feature-box ';if ($entradaactual == "IN1") echo "alert-info"; echo '">
                                <span class="icon number">1</span>
                                <h4>Canal 1</h4>
                                <p>Entrada 1 de la placa.</p>
                                <p><b>Canal local TCC1</b></p>
                                <br>
                                <br>
                                <div class="material-switch row">
                                        <div class="col-md-12 button-position">
                                                <input id="canal1" name="in1" type="checkbox" class="canales" onClick="CambiaCanal(\'cambiar.php?opc=canal0\', \'Cambiando Canal...\')" '; if ($entradaactual == "IN1") echo "checked disabled"; echo '/>
                                                <label for="canal1" class="label-primary"></label>
                                        </div>
                                </div>
                              </div>
                            </div> <!-- End Col -->

                            <div class="col-xs-4 col-sm-4 col-md-4">
                                <div class="feature-box ';if ($entradaactual == "IN2") echo "alert-info"; echo '">
                                <span class="icon number">2</span>
                                <h4>Canal 2</h4>
                                <p>Entrada 2 de la placa.</p>
                                <p>Dispositivo: <b>Medialink</b></p>
                                <br>
                                <br>
                                <div class="material-switch row">
                                        <div class="col-md-12 button-position">
                                                <input id="canal2" name="in2" type="checkbox" class="canales" onClick="CambiaCanal(\'cambiar.php?opc=canal1\', \'Cambiando Canal...\')" ';
                                                if ($entradaactual == "IN2") {
                                                  echo "checked disabled";
                                                }else{
                                                  echo $entradaactual;
                                                }
                                                echo '/><label for="canal2" class="label-primary"></label>
                                        </div>
                                </div>
                                </div>
                            </div> <!-- End Col -->
                            <div class="col-xs-4 col-sm-4 col-md-4">
                            <div class="feature-box ';if ($entradaactual == "IN3") echo "alert-info"; echo '">
                            <span class="icon number">3</span>
                            <h4>Canal 3</h4>
                            <p>Entrada 3 de la placa.</p>
                            <p>Canal en negro</p>
                            <br>
                            <br>
                            <div class="material-switch row">
                                    <div class="col-md-12 button-position">
                                            <input id="canal3" name="in3" type="checkbox" class="canales" onClick="CambiaCanal(\'cambiar.php?opc=canal2\', \'Cambiando Canal...\')" '; if ($entradaactual == "IN3") echo "checked disabled"; echo '/>
                                            <label for="canal3" class="label-primary"></label>
                                    </div>
                            </div>
                            </div>
                            </div> <!-- End Col -->

                          </div>
                </div>
                </div>
                <script>
                $(document).ready(function() {

                  function update() {
                    $.ajax({
                     type: \'POST\',
                     url: \'estadoEncrip.php\',
                     timeout: 1000,
                     success: function(data) {
                        $("#simulcrypt").html(data);
                        window.setTimeout(update, 1000);
                     }
                    });
                   }
                   update();

                });
                </script>
                ';

?>
