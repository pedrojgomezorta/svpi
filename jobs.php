<?php include 'includes/config.inc.php'; ?>
<?php include 'html/head.html.php'; ?>
<?php include 'html/menu.html.php'; ?>
<?php menu("Jobs"); ?>

<!--Container-->

<br>

<button type="button" class="btn btn-danger pull-right" data-toggle="modal" data-target="#borrar">Limpiar todas las tareas</button>

<!-- Modal -->
<div class="modal fade" id="borrar" tabindex="-1" role="dialog" aria-labelledby="borrarLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="BorrarLabel">Borrar tareas</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      ¿Desea borrar todas las tareas?
      </div>
      <div class="modal-footer">
      <form method="post" action="jobs.php">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <input type="submit" class="btn btn-danger" name="borrarTareas" value="Limpiar la lista de tareas">
      </form>
      </div>
    </div>
  </div>
</div>';

<!-- Button trigger modal -->
<button type="button" class="btn pull-left btn-info" data-toggle="modal" data-target="#exampleModal">
  Programar tarea
</button>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">Programar nueva tarea</h4>
        <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>-->
        </button>
      </div>
      <div class="modal-body">
      <form method="post" action="jobs.php">';




          //Dia de la semana
          $dow = ['lunes','martes','miercoles','jueves','viernes','sabado','domingo'];

          echo '<div class="form-group">
                <label for="dow">Día de la semana actual: ('.$dow[date('w')-1].')</label>
                <select class="form-control" id="dow" name="dow">';
                $dow = ['lunes','martes','miercoles','jueves','viernes','sabado','domingo'];
                for ($i=0; $i < count($dow); $i++) {
                  if ($i+1 == date('w')) {
                    echo "<option value=".($i+1)." selected>".$dow[$i]." (hoy)</option>";
                  }else{
                  echo "<option value=".($i+1).">".$dow[$i]."</option>";
                  }
                }
                echo '</select>
                      </div>';

          //Canal
          echo '<div class="form-group">
                <label for="canal">Canal a cambiar</label>
                <select class="form-control" id="canal" name="canal">
                  <option value="1">Canal 1</option>
                  <option value="2">Canal 2</option>
                  <option value="3">Canal 3</option>
                </select>
                </div>';
          echo '<div class="form-group">
                  <label for="encriptado">¿Encriptar el canal? <i class="fas fa-lock"></i> / <i class="fas fa-lock-open"></i></label>
                  <select class="form-control" id="encriptado" name="encriptado">
                    <option value="1">Si (Encriptar canal)</option>
                    <option value="0">No (Desencriptar canal)</option>
                  </select>
                  </div>';

        echo '</div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <input type="submit" class="btn btn-primary" value="Programar tarea" name="envio">
      </div>
      </form>
    </div>
  </div>
</div><br><br><br>';
echo '<div class="well">
    <table class="table">
      <thead>
        <tr>
          <th>Dia / Hora</th>
          <th>Tarea</th>
          <th>Info</th>
        </tr>
      </thead>
      <tbody>
        ';
        leerTareas($con);
        echo '
      </tbody>
    </table>
</div>
</div>';

<?php include 'html/footer.html'; ?>
